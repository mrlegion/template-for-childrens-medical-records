﻿namespace MKC_Logic.Helpers
{
    public class MedicalCard
    {
        public int InsurancePolicyNumber { get; set; }

        public int CardNumber { get; set; }

        public BloodGroup BloodGroup { get; set; }

        public RhesusFactor RhesusFactor { get; set; }

        public Gender Gender { get; set; }

        public int KindArea { get; set; }

        public bool UseKindArea { get; set; }
    }
}