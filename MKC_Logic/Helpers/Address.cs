﻿namespace MKC_Logic.Helpers
{
    public class Address
    {
        public string City { get; set; }

        public string Street { get; set; }

        public int HouseNumber { get; set; }

        public string SuffixHouseNumber { get; set; }

        public int Apartment { get; set; }
    }
}