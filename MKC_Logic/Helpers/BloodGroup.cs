﻿namespace MKC_Logic.Helpers
{
    public enum BloodGroup
    {
        None = 0,
        One = 1,
        Two = 2,
        Three = 3,
        Four = 4
    }
}