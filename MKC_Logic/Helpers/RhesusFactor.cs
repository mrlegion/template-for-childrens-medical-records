﻿namespace MKC_Logic.Helpers
{
    public enum RhesusFactor
    {
        None = 0,
        Negative = 1,
        Positive = 2
    }
}