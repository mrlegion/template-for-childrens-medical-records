﻿namespace MKC_Logic.Helpers
{
    public enum Institution
    {
        None = 0,
        Kindergarden = 1,
        School = 2
    }
}