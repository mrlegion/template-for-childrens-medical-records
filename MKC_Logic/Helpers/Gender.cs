﻿namespace MKC_Logic.Helpers
{
    public enum Gender
    {
        None = 0,
        Boy = 1,
        Girl = 2
    }
}