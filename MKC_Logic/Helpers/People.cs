﻿using System;

namespace MKC_Logic.Helpers
{
    public class People
    {
        public string FirstName { get; set; }

        public string SecondName { get; set; }

        public string LastName { get; set; }

        public DateTime Birthday { get; set; }
    }
}