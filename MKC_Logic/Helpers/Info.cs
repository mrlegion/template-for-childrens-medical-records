﻿namespace MKC_Logic.Helpers
{
    public class Info
    {
        public People People { get; set; }

        public Address Address { get; set; }

        public MedicalCard MedicalCard { get; set; }

        public string MobilePhone { get; set; }

        public string HomePhone { get; set; }

        public Institution Institution { get; set; }

        public int InstitutionNumber { get; set; }

        public bool UseParentLabel { get; set; }
    }
}